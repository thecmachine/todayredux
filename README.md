## Clint Questions The App!

I designed this to showcase a bit of Laravel 5.6 latest and greatest.

I used Laravel - Valet on my local machine and I very much recommend it!



You can Register a User, Login and Answer randomly seeded Questions with random answers once Daily.

You can View Previously Answered Daily Questions.

Simple.

## Future Features
* Admin User and Dashboard with Events for Mocking Demonstration

## Install

* Clone the [Repository](https://bitbucket.org/thecmachine/todayredux) into a VirtualServer Directory Locally
```
$ git clone https://thecmachine@bitbucket.org/thecmachine/todayredux.git
```

* Composer 
```
$ composer install
```

* NPM 
```
$ npm install
```

* Yarn 
```
$ yarn run watch
```

Refresh and Seed Database 
```
$ php artisan migrate:fresh --seed
```

* Edit .env DB_DATABASE, DB_USERNAME, and DB_PASSWORD Variables for Local DB

## Testing
* Dusk Browser Tests
```
$ php artisan dusk 
```

* PHPUnit Test Suite
```
$ vendor/bin/phpunit
```

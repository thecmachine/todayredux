<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PagesTest extends TestCase
{
    /**
     * A basic landing page test.
     *
     * @return void
     */
    public function testLandingPage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    
    /**
     * A basic register page test.
     *
     * @return void
     */
    public function testRegisterPage()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }
    
    /**
     * A basic login page test.
     *
     * @return void
     */
    public function testLoginPage()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }
    
     /**
     * A user route.
     *
     * @return void
     */
    public function testUserRoute()
    {
	    $user = factory(\App\User::class)->create(); 
		$this->be($user); 
        $response = $this->get('/user/' . $user->id);

        $response->assertStatus(200);
    }
    
}

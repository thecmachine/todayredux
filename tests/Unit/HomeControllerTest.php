<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeControllerTest extends TestCase
{
	
	//We Can unit test Laravels make:auth routes another time ;P
	
    /**
     * Test User GET API Route.
     *
     * @return void
     */
    public function testUserRoute()
    {
		$user = factory(\App\User::class)->create(); 
		$this->be($user); 
	    
	    $this->json('get', '/user/' . $user->id)
	    	->assertStatus(200);
    }
     
    /**
     * Test User Route Return Data Format .
     *
     * @return void
     */
    public function testUserRouteReturnDataFormat()
    {
		$user = factory(\App\User::class)->create(); 
		$this->be($user); 
	    
	    $this->json('get', '/user/' . $user->id)
	    	->assertStatus(200)
	    	->assertJsonStructure([
		    	'data' => [
			    	'name',
			    	'email',
		    	], 
		    	'dailyComplete',
		    	'version'
	    	]);
    }
    
    /**
     * Test User Route Returns dailyComplete False - When questions arent answered
     *
     * @return void
     */
    public function testUserRouteReturnsDailyCompleteFalse()
    {
		$user = factory(\App\User::class)->create(); 
		$this->be($user); 
	    
	    $this->json('get', '/user/' . $user->id)
	    	->assertStatus(200)
	    	->assertJsonStructure([
		    	'data' => [
			    	'name',
			    	'email',
		    	], 
		    	'dailyComplete',
		    	'version'
	    	])
	    	->assertJson([
		    	'dailyComplete' => false
	    	]);
    }
    
    /**
     * Test User Route Returns dailyComplete True - When questions are answered
     *
     * @return void
     */
    public function testUserRouteReturnsDailyCompleteTrue()
    {
		$user = factory(\App\User::class)->create(); 
		$this->be($user); 
		//populate some responses for our new user
		//if i needed to use this again id abstract it to a _setup() function for DRY purposes
		$question = \App\Question::first();
		$answer = \App\Answer::first();
		factory(\App\Response::class)->create([
		    "user_id" => $user->id,
			"answer_id" => $answer->id,
			"question_id" => $question->id,
			"created_at" => new \DateTime(),
		]); 
	    
	    $this->json('get', '/user/' . $user->id)
	    	->assertStatus(200)
	    	->assertJsonStructure([
		    	'data' => [
			    	'name',
			    	'email',
		    	], 
		    	'dailyComplete',
		    	'version'
	    	])
	    	->assertJson([
		    	'dailyComplete' => true
	    	]);
    }
}

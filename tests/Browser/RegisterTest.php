<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Response;
use App\Question;
use App\Answer;
use DB;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk Home View test.
     *
     * @return void
     */
    public function testHomeView()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Clint');
        });
    }
    
     /**
     * A Dusk Home View Register Link Click test.
     *
     * @return void
     */
    public function testHomeViewRegisterLinkClick()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->clickLink('Register')
                    ->assertSee('Register');
        });
    }
    
    /**
     * A Dusk Home View Register Link Click test Fill Out And Submit Form.
     *
     * @return void
     */
    public function testFillOutAndSumbitForm()
    {
        $this->browse(function (Browser $browser) {          
            $browser->visit('/')
                    ->clickLink('Register')
                    ->assertSee('Register')
                    ->value('#name', str_random(8))
                    ->value('#email', str_random(12).'@mail.com')
                    ->value('#password', '123456')
                    ->value('#password-confirm', '123456')
                    ->click('button[type="submit"]')
                    ->assertPathIs('/home');
        });
    }
	 
	 /**
     * Daily Questions Incomplete Show Daily Questions
     *
     * @return void
     */
    public function testLoginAndViewDailyQUestions()
    {
        $this->browse(function (Browser $browser) {
	        
	        $user = factory(\App\User::class)->create(); 
	              
			/*
	        $todaysResponses = Response::where('user_id', $user->id)->whereDate('created_at', DB::raw('CURDATE()'))->get();
	        count($todaysResponses);  //will be 0 if no answers for today
			*/
	       
            $browser->loginAs($user->id)
                    ->visit('/home')
                    ->assertSee('Daily Questions')
                    ->assertSee('History');
        });
    }
     
 	/**
     *  A Dusk Daily Questions Answered Answers Displayed
     *
     * @return void
     */
    public function testLoginAndViewAnswerHistory()
    {
        $this->browse(function (Browser $browser) {
	        
	        $user = factory(\App\User::class)->create(); 
	        
	        
	        //populate some responses for our new user
	        //if i needed to use this again id abstract it to a _setup() function for DRY purposes
	        $question = Question::first();
	        $answer = Answer::first();
	        factory(\App\Response::class)->create([
		        "user_id" => $user->id,
				"answer_id" => $answer->id,
				"question_id" => $question->id,
				"created_at" => new \DateTime(),
	        ]); 	        

			/*
	        $todaysResponses = Response::where('user_id', $user->id)->whereDate('created_at', DB::raw('CURDATE()'))->get();
	        count($todaysResponses);  //will be > 0 if answers for today
			*/
	       
            $browser->loginAs($user->id)
                    ->visit('/home')
                    ->assertSee('Daily Questions')
                    ->assertSee('History')
                    ->assertSee('You Answered Questions Today!');
        });
    }
    
   
    
   
    
    
}

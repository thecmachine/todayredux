<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Response::class, function (Faker $faker) {
    return [
        'question_id' => $faker->randomDigit,
        'answer_id' => $faker->randomDigit,
        'user_id' => $faker->randomDigit,
    ];
});

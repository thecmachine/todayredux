<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Question::class, function (Faker $faker) {
	$text = $faker->unique()->realText(55);
	$arr = explode(' ', trim($text));
    $arr[0]; //first word only
    return [
        'copy' => $text,
        'slug' => $arr[0],
    ];
});

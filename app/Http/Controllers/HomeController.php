<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\User as UserResource;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
	 /**
     * Show a random User JSON
     *
     * @return \Illuminate\Http\Response
     */
	public function user($id)
	{
		$user = User::find($id);
		if($user){
			return new UserResource($user);
		}else{
			return json_encode(["Error" => "Non-valid User Id"]);
		}
	}
}

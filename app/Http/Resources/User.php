<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Response;
use DB;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {	
	    return [
		  	'name' => $this->name,
		  	'email' => $this->email,  
	    ];
    }
    
    //including some extra data 
    public function with($request){
	    $todaysResponses = count(Response::where('user_id', $this->id)->whereDate('created_at', DB::raw('CURDATE()'))->get()) > 0 ? true : false;
	    return [
		    'dailyComplete'=> $todaysResponses,
		    'version' => '1.0',
	    ];
	    
    }
}
